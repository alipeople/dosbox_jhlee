const { app, BrowserWindow, ipcMain } = require('electron');

app.on('ready', () => {

	win = new BrowserWindow({
	//	fullscreen : true,
	//	kiosk: true,
	show: true,
	fullscreenable: true,
	backgroundColor: '#131313',
	title: 'DOSBOX 에뮬레이터',
	width: 1024,
	height : 768,
	minWidth: 1024,
	minHeight: 768,
	//	frame:false,
	autoHideMenuBar: true,
	webPreferences: {
		nodeIntegration: true,
		contextIsolation : false,
		webSecurity : false
	}
	})

	//브라우저창이 읽어 올 파일 위치
	win.loadFile('./index.html')
	//	win.maximize();

});