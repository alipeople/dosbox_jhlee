
//	스크롤바 시작
OverlayScrollbars(document.querySelectorAll('body'), { 
	className : "os-theme-light",
	scrollbars : {
		autoHide: "leave"
	}
});

// 이미지 리사이저
function resizedataURL(datas, index, wantedWidth, wantedHeight)
{
	img = document.createElement('img');
	img.src = datas[index].poster;
	img.onload = function()
	{        
		this.style.width = wantedWidth;
		this.style.height = wantedHeight;
		var canvas = document.createElement('canvas');
		var ctx = canvas.getContext('2d');
		canvas.width = wantedWidth;
		canvas.height = wantedHeight;
		ctx.drawImage(this, 0, 0, wantedWidth, wantedHeight);

		ctx.fillStyle='#DB1019';
		ctx.globalAlpha = 0.9;
		ctx.fillRect(0,(wantedHeight - 30), (wantedWidth - 20),20);
		ctx.fill();

		ctx.font = 'bold 10pt Arial';
		ctx.textAlign = 'left';
		ctx.fillStyle = 'white';
		ctx.fillText((datas[index].title).toUpperCase(), 10, (wantedHeight - 16));

		var dataURI = canvas.toDataURL();
		datas[index].poster = dataURI;
	};
}

function lightbox(conf, html, callback){

	// 환경설정 제어
	conf			=	(typeof conf			!=	"object"   )	?	{}			:	conf;
	conf			=	(typeof conf			==	"undefined")	?	{}			:	conf;
	conf.bgcolor	=	(typeof conf.bgcolor	==	"undefined")	?	"#000000"	:	conf.bgcolor;
	conf.opacity	=	(typeof conf.opacity	==	"undefined")	?	"0.45"		:	conf.opacity;
	conf.selector	=	(typeof conf.selector	==	"undefined")	?	"custom"	:	conf.selector;
	conf.classNM	=	(typeof conf.classNM	==	"undefined")	?	""			:	conf.classNM;
	HtmlContents	=	(typeof html			==	"undefiled")	?	"&nbsp;"	:	html;

	//	ARGB 일 경우 Alpha 값 구하기 :: 하위 버젼 익스브라우져 호환
	var	avars	=	Math.round(parseFloat(conf.opacity) *	100)	/	100;
	var alpha	=	Math.round(parseFloat(conf.opacity)	*	255);
	var ahex	=	(alpha + 0x10000).toString(16).substr(-2).toUpperCase();

	//	옵션파악
	if(typeof html == "boolean")	{	$("."+conf.selector).remove(); return false; }

	//	중복 레이어는 제거합니다.
	$("."+conf.selector).remove();

	// 레이아웃 생성후 데이터 넣기
	$("<div class='lightbox " + conf.selector + " " + conf.classNM + "' style='position:fixed;top:0;left:0;width:100%;height:100%;background-image:url(images/mask.png);display:table;z-index:20;'><div id='______LightboxView' class='__CloseThis' style='display:table-cell;vertical-align: middle;'></div></div>").appendTo("body");

	//	익스 하위 시리즈는 스크롤이 생기지 않는다.
	$("."+conf.selector).find("#______LightboxView").html(HtmlContents);

	//	클로즈 버튼 클릭시 레이어창 닫기 - 특정 일러먼트만
	$("body").find(".lightbox").off("click.__closeThis").on("click.__closeThis", ".__CloseThis", function(e){

		//  창닫기
		if($(e.target).attr("id") == '______LightboxView')
		{
			if (typeof callback == "function") { callback(); }
			$("."+conf.selector).remove();
		}

	});

	//	클로즈 버튼 클릭시 레이어창 닫기
	$("body").find(".lightbox").off("click.__lightbox").on("click.__lightbox", ".__CloseMe", function(){
		if (typeof callback == "function") { callback(); }
		$("."+conf.selector).remove();
	});

}

//	템플릿 엔진 참조
const Mustache = require('mustache');
const sqlite3 = require('sqlite3').verbose();
const path = require('path');

const dbFile = path.join(__dirname, '\database.db').replace('\app.asar', '');
const db = new sqlite3.Database(dbFile);

//	변수 목록 저장
var rowData = {'acade' : [], 'adventure' : [], 'simulation' : [], 'rpg' : [], 'sport' : []};

//	게임 리스트 갱신하기
function getGameList() 
{
    db.serialize(function () {

		var i = 0;		
        db.each("SELECT * FROM game_info WHERE category = 'Action' ORDER BY play DESC, seq ASC LIMIT 0, 12", function (err, row) {
			console.log(row);
			rowData['acade'][i] = row;
			resizedataURL(rowData['acade'], i, 288, 156);
			i++;
        });

		//	템플릿 갱신
		setTimeout(function(){
			template = document.querySelector("#acade-template").innerHTML;
			document.querySelector("#acade-rows").innerHTML = Mustache.render(template, rowData);
		}, 700);

    });

	//	어드벤쳐
    db.serialize(function () {

		var i = 0;		
        db.each("SELECT * FROM game_info WHERE category = 'Adventure' ORDER BY play DESC, seq ASC LIMIT 0, 12", function (err, row) {

			rowData['adventure'][i] = row;
			resizedataURL(rowData['adventure'], i, 288, 156);
			i++;
        });

		//	템플릿 갱신
		setTimeout(function(){
			template = document.querySelector("#adventure-template").innerHTML;
			document.querySelector("#adventure-rows").innerHTML = Mustache.render(template, rowData);
		}, 700);

    });

	//	시뮬레이션
    db.serialize(function () {

		var i = 0;		
        db.each("SELECT * FROM game_info WHERE category = 'Simulation' ORDER BY play DESC, seq ASC LIMIT 0, 12", function (err, row) {

			rowData['simulation'][i] = row;
			resizedataURL(rowData['simulation'], i, 288, 156);
			i++;
        });

		//	템플릿 갱신
		setTimeout(function(){
			template = document.querySelector("#simulation-template").innerHTML;
			document.querySelector("#simulation-rows").innerHTML = Mustache.render(template, rowData);
		}, 700);

    });

	//	롤플레잉
    db.serialize(function () {

		var i = 0;		
        db.each("SELECT * FROM game_info WHERE category = 'Rpg' ORDER BY play DESC, seq ASC LIMIT 0, 12", function (err, row) {

			rowData['rpg'][i] = row;
			resizedataURL(rowData['rpg'], i, 288, 156);
			i++;
        });

		//	템플릿 갱신
		setTimeout(function(){
			template = document.querySelector("#rpg-template").innerHTML;
			document.querySelector("#rpg-rows").innerHTML = Mustache.render(template, rowData);
		}, 700);

    });

	//	스포츠
    db.serialize(function () {

		var i = 0;		
        db.each("SELECT * FROM game_info WHERE category = 'Sport' ORDER BY play DESC, seq ASC LIMIT 0, 12", function (err, row) {

			rowData['sport'][i] = row;
			resizedataURL(rowData['sport'], i, 288, 156);
			i++;
        });

		//	템플릿 갱신
		setTimeout(function(){
			template = document.querySelector("#sports-template").innerHTML;
			document.querySelector("#sports-rows").innerHTML = Mustache.render(template, rowData);
		}, 700);

    });

}

getGameList();

//	db.close();

//	로딩 처리
$('body').off('click.__games').on('click.__games', '.games', function(){

	var that = $(this);
	lightbox({'selector' : 'game'}, '<div style="width:80%; height:80%; background-color:#FFFFFF; margin:auto; border:1px #000000 solid;"><iframe id="GameCanvas" frameborder="0px" style="width:100%; height:100%;"></iframe></div>');
	if(that.attr('game'))
	{
		$('#GameCanvas').attr('src', 'https://dos.zone/en/player/' + encodeURIComponent(that.attr('game')));
	} else {
		$('#GameCanvas').attr('src', 'https://dos.zone/en/player/' + encodeURIComponent('https://cdn.dos.zone/original/2X/1/1179a7c9e05b1679333ed6db08e7884f6e86c155.jsdos'));
	}

	//	포커스 고정
	setTimeout(function(){ document.getElementById("GameCanvas").focus(); }, 1000);

});